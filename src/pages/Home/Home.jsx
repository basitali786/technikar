import React from "react";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";
import HowWeAre from "../../components/HowWeAre/HowWeAre";
import Services from "../../components/Services/Services";
import Mission from "../../components/Mission/Mission";

const Home = () => {
  return (
    <>
      <Navbar />
      <HowWeAre />
      <Mission />
      <Services />
      <Footer />
    </>
  );
};

export default Home;
