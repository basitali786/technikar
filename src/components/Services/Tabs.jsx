import React from "react";

const Tabs = ({ src, text, paragraph }) => {
  return (
    <div className="Tabs-services ">
      <div className="Tabs-services-wrapper ">
        <div className="container">
          <div className="row ">
            <div className="col-lg-6">
              <img src={src} />
            </div>
            <div className="col-lg-6 text-left position-relative">
              <h2 className="text-white divider-heading">{text}</h2>
              <p className="mt-4 text-white">{paragraph}</p>
              <ul className="custom-timeline mt-4">
                <li className="first">
                  <span></span>
                  Deploying seamless integration in applications to automate
                  workflows.
                </li>
                <li>
                  <span></span>
                  Salient UI to boost customer engagement and acquisition.
                </li>
                <li>
                  <span></span>
                  Instinctive UX to prevent user errors and reduce bounce rate.
                </li>
                <li class="last">
                  <span></span>
                  Sustainable performance to ensure user satisfaction.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tabs;
