import React, { useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./Services.css";
import Tabs from "./Tabs";

export default () => {
  const [activeTab, setActiveTab] = useState(0);

  const handleTabSelect = (index) => {
    setActiveTab(index);
  };

  const src = "https://www.fossphorus.com/assets/images/ser/img4.png";
  const text1 = "School Management System";
  const text2 = "Graphic Designer";
  const text3 = "Hospital Management System";
  const text4 = "E-commerce Marketing";
  const text5 = "Restaurant Management System";
  const text6 = "Customer Care Management";
  const text7 = "Product Shoot";
  const text8 = "Order Management Services";
  const text9 = "Social Media Marketing";
  const text10 = "Hospital Management System";
  const paragraph1 =
    "Tochnikar gives well developed and fully functional school ERP at a reasonable price. Technikar ERP has 25+ incredible modules that will be made useful for school at each level to development, and the management structure foundations for learning, administration, and management";

  const sliderSettings = {
    dots: false,
    infinite: false,
    slidesToShow: 5,
    // fade: true,
  };

  return (
    <div className="services_swiper">
      <div className="container">
        <Slider {...sliderSettings}>
          <a
            className={activeTab === 0 ? "active" : ""}
            onClick={() => handleTabSelect(0)}
          >
            School Management <br />
            System
          </a>
          <a
            className={activeTab === 1 ? "active" : ""}
            onClick={() => handleTabSelect(1)}
          >
            Social Media <br />
            Marketing
          </a>
          <a
            className={activeTab === 2 ? "active" : ""}
            onClick={() => handleTabSelect(2)}
          >
            Hospital Management <br />
            System
          </a>
          <a
            className={activeTab === 3 ? "active" : ""}
            onClick={() => handleTabSelect(3)}
          >
            E-commerce <br />
            Marketing
          </a>
          <a
            className={activeTab === 4 ? "active" : ""}
            onClick={() => handleTabSelect(4)}
          >
            Restaurant <br /> Management
          </a>
          <a
            className={activeTab === 5 ? "active" : ""}
            onClick={() => handleTabSelect(5)}
          >
            Graphic <br />
            Designing
          </a>
          <a
            className={activeTab === 6 ? "active" : ""}
            onClick={() => handleTabSelect(6)}
          >
            Customer Care <br />
            Management
          </a>
          <a
            className={activeTab === 7 ? "active" : ""}
            onClick={() => handleTabSelect(7)}
          >
            Product <br />
            Shoot
          </a>
          <a
            className={activeTab === 8 ? "active" : ""}
            onClick={() => handleTabSelect(8)}
          >
            Order Management <br />
            Services
          </a>
          <a
            className={activeTab === 9 ? "active" : ""}
            onClick={() => handleTabSelect(9)}
          >
            Fashion Campaign <br />
            Photography
          </a>
        </Slider>

        <div className="tab-content">
          {activeTab === 0 && (
            <Tabs src={src} text={text1} paragraph={paragraph1} />
          )}
          {activeTab === 1 && (
            <Tabs src={src} text={text2} paragraph={paragraph1} />
          )}
          {activeTab === 2 && (
            <Tabs src={src} text={text3} paragraph={paragraph1} />
          )}
          {activeTab === 3 && (
            <Tabs src={src} text={text4} paragraph={paragraph1} />
          )}
          {activeTab === 4 && (
            <Tabs src={src} text={text5} paragraph={paragraph1} />
          )}
          {activeTab === 5 && (
            <Tabs src={src} text={text6} paragraph={paragraph1} />
          )}
          {activeTab === 6 && (
            <Tabs src={src} text={text7} paragraph={paragraph1} />
          )}
          {activeTab === 7 && (
            <Tabs src={src} text={text8} paragraph={paragraph1} />
          )}
          {activeTab === 8 && (
            <Tabs src={src} text={text9} paragraph={paragraph1} />
          )}
          {activeTab === 9 && (
            <Tabs src={src} text={text10} paragraph={paragraph1} />
          )}
        </div>
      </div>
    </div>
  );
};
