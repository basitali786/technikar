import React from "react";
import "./Services.css";
import ServicesTabs from "./ServicesTabs";

const Services = () => {
  return (
    <div className="services">
      <div className="services-wrapper ">
        <div className="services-top-section mb-5 text-center">
          <h2 className="text-white ">Our Services</h2>
          <p className="text-white">
            We blend strategic thinking, technical proficiency, and unparalleled
            artistry to craft integrated digital marketing solutions.
          </p>
        </div>
        <ServicesTabs />
      </div>
    </div>
  );
};

export default Services;
