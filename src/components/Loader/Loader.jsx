import React from "react";
import "./Loader.css";

const Loader = () => {
  const svgCode = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgb(241 242 243 / 0%); display: block;" width="300" height="300" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
    <text x="50" y="50" text-anchor="middle" dy="0.38em" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke="#F91A10" stroke-width="0.7px" font-size="21" font-family="arial">
      Technikar
      <animate attributeName="stroke-dasharray" repeatCount="indefinite" calcMode="spline" dur="5.5555555555555545s" values="0 100;100 100;0 100" keyTimes="0;0.5;1" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" stroke="#F91A10" fill="none" stroke-width="0.7px"></animate>
      <animate attributeName="stroke-dashoffset" repeatCount="indefinite" dur="5.5555555555555545s" values="0;0;-100" keyTimes="0;0.5;1" stroke="#F91A10" fill="none" stroke-width="0.7px"></animate>
    </text>
  </svg>`;

  return (
    <div className="loader-container">
      <div className="loader" dangerouslySetInnerHTML={{ __html: svgCode }} />
    </div>
  );
};

export default Loader;
