import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

const SocialIcons = () => {
  return (
    <div className="left_icons float-left d-table" data-aos="fade-down">
      <div className="icon_content d-table-cell align-middle">
        <ul className="list-unstyled p-0 m-0">
          <li className="hover-effect">
            <a href="#">
              <FontAwesomeIcon icon={faInstagram} />
            </a>
          </li>
          <li className="hover-effect">
            <a href="#">
              <FontAwesomeIcon icon={faTwitter} />
            </a>
          </li>
          <li className="p-0 hover-effect">
            <a href="#">
              <FontAwesomeIcon icon={faFacebook} />
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default SocialIcons;
