import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

const BannerContent = () => {
  const text = "WELCOME TO TECHNIKAR SOLUTIONS";
  const text2 = "We are Creative Digital Agency";
  const paragraph =
    "Coming Together Is A Beginning, Staying Together Is Progress, And Working Together Is Success.";
  const btext = "Contact Us";
  const btext2 = "Read More";
  return (
    <>
      <h6>{text}</h6>
      <h1>{text2}</h1>
      <p className="text-white text-capitalize">{paragraph}</p>
      <a className="text-decoration-none default-btn hover-effect contactus_btn mx-3">
        {btext}
        <FontAwesomeIcon className="mx-2" icon={faArrowRight} />
      </a>
      <a className="text-decoration-none default-btn hover-effect readmore_btn">
        {btext2}
        <FontAwesomeIcon className="mx-2" icon={faArrowRight} />
      </a>
    </>
  );
};

export default BannerContent;
