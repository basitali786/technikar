import React from "react";
import BannerContent from "./BannerContent";
import "./Banner.css";
import SocialIcons from "./SocialIcons";

const Banner = () => {
  return (
    <div className="banner-section position-relative">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="banner-section-content text-center">
              <BannerContent />
              <SocialIcons />
            </div>
          </div>
        </div>
        <figure className="shape4 mb-0 position-absolute">
          <img src="images/banner_shape4.png" />
        </figure>
      </div>
    </div>
  );
};

export default Banner;
