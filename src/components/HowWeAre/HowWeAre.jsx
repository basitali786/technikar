import React from "react";
import "./HowWeAre.css";

const HowWeAre = () => {
  return (
    <div className="aboutus_section position-relative">
      <div className="aboutus_section_wrapper">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-12 col-sm-12 col-12">
              {" "}
              <div className="about_images position-relative">
                <figure className="mb-0 about_img position-relative">
                  <img
                    src="images/aboutus_image.jpg"
                    alt=""
                    class="img-fluid"
                  />
                </figure>
                <figure className="mb-0 shape1 position-absolute">
                  <img
                    src="images/aboutus_image_shape1.jpg"
                    alt=""
                    class="img-fluid"
                  />
                </figure>
                <figure className="mb-0 shape2 position-absolute shape_2">
                  <img
                    src="images/aboutus_image_shape2.png"
                    alt=""
                    class="img-fluid"
                  />
                </figure>
                <div class="aboutus_img_box position-absolute">
                  <figure>
                    <img
                      src="images/aboutus_box_shape.png"
                      alt=""
                      class="img-fluid"
                    />
                  </figure>
                  <div class="span_wrapper">
                    <span class="number counter">60% Males</span>
                    <span class="number counter">40% Females</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12 col-12 d-flex align-items-center">
              <div class="heading" data-aos="fade-right">
                <div class="line_wrapper position-relative">
                  <h2>How We Work</h2>
                  <figure class="mb-0 position-absolute">
                    <img
                      src="./assets/images/red_line.png"
                      alt=""
                      class="img-fluid"
                    />
                  </figure>
                </div>
                <div>
                  <h5>1. Technology Driven Innovation</h5>
                  <p>
                    Technology-driven innovation as an advanced discipline of
                    rapid technology experime- ntation that applies rigor,
                    resources, and customer in sight to find, prepare for, and
                    commercialize disruptive, technology-fueled change.
                  </p>
                </div>
                <div>
                  <h5>2. Results-oriented</h5>
                  <p>
                    Technikar focuses on the out come with the process. We give
                    importance to developmental experimentation and software
                    echancement and the procedure of it all.
                  </p>
                </div>
                <div>
                  <h5>3. Human-centered</h5>
                  <p>
                    We put the user's needs, desires, and abilities at the
                    center of the development process. It means making design
                    decisions based on how a consumer/client can need and want
                    their brands to be, rather than expecting users to adjust
                    and accommodate to the beha- viours to the product.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <figure class="mb-0 right_shape position-absolute shape_2">
            <img src="images/about_right_shape.png" alt="" class="img-fluid" />
          </figure>
        </div>
      </div>
    </div>
  );
};

export default HowWeAre;
