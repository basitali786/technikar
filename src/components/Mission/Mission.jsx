import React from "react";
import "./Mission.css";
import Box from "./Box";

const Mission = () => {
  const title1 = "THE MISSION";
  const title2 = "WHO WE ARE";
  const img1 =
    "https://html.designingmedia.com/blazin/assets/images/mission_img.png";
  const img2 =
    "https://html.designingmedia.com/blazin/assets/images/vision_img.png";
  const para2 =
    "Founded by IT professionals and business experts, TechniKar Solutions, Inc. (TSI) has been a trusted provider of strategic IT services and solutions since its inception. Over time, we've successfully catered to a diverse clientele, including Northwest Airline and Taco Bell, with services ranging from web development to e-commerce. Recognizing the dynamic technological landscape, TSI expanded its offerings to include social media management and e-commerce performance marketing. This marked our evolution from a pure IT service provider to a comprehensive digital solution company. ";
  const para1 =
    "TechnKar Solutions’ primary mission is to provide a one-stop solution for Information Technology issues for our clients, at a minimum cost, making use of global resources to fulfill these requests. The company shall establish business alliances with strong, aggressive, and rapidly growing IT service organizations on a worldwide basis to insure the ability to deliver minimum-cost, highquality services and solutions to its client base. This shall ensure that TechniKar always has the expertise and manpower necessary to deliver solutions based on emerging technologies that would provide its clients with competitive advantages in their industries";
  return (
    <div className="mission_vision_section">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-12 col-12 mb-md-0 mb-4">
            <Box title={title1} para={para1} img={img1} />
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12 col-12 mb-md-0 mb-4">
            <Box title={title2} para={para2} img={img2} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Mission;
