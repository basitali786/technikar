import React from "react";

const Box = ({ title, para, img }) => {
  return (
    <div className="box">
      <div className="content">
        <div className="image_wrapper">
          <figure>
            <img src={img} />
          </figure>
        </div>
        <h4 className="text-white">{title}</h4>
        <p className="text-size-16 mb-0 text-white">{para}</p>
      </div>
    </div>
  );
};

export default Box;
