import React from "react";
import "./Navbar.css";
import Toggle from "./Toggle";
import NavLinks from "./NavLinks";
import Logo from "./Logo";
import Banner from "../Banner/Banner";

const Navbar = () => {
  return (
    <div className="banner-section-outer position-relative position-sticky">
      <header>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light sticky-top">
            <Logo />
            <Toggle />
            <NavLinks />
          </nav>
        </div>
      </header>
      <Banner />
    </div>
  );
};

export default Navbar;
