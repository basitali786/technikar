import React from "react";
const Logo = () => {
  return (
    <a class="navbar-brand" href="#">
      <figure class="mb-0">
        <img src="images/blazin_logo.png" alt="" class="img-fluid" />
      </figure>
    </a>
  );
};

export default Logo;
