import React from "react";

const NavLinks = () => {
  return (
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="#">
            Home
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            About
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            Services
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            Contact
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            Our Team
          </a>
        </li>
      </ul>
      <div class="outer-div position-relative float-left">
        <figure class="user-img mb-0">
          <img class="img-fluid" src="images/callus_icon.png" alt="" />
        </figure>
        <div class="callus_outer position-absolute">
          <span class="text-white">Call us now:</span>
          <a href="tel:+012(345)6789" class="text-decoration-none text-white">
            +012 (345) 6789
          </a>
        </div>
      </div>
    </div>
  );
};

export default NavLinks;
